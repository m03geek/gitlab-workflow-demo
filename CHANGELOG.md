#### 1.3.6 (2017-08-21)

##### Continuous Integration

* **all:** remove tags ([b755da4a](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/b755da4a664ae51eff9cf0cb29264d31e619e35c))

#### 1.3.5 (2017-08-21)

##### Continuous Integration

* **npm:** fix auth ([c79c367a](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/c79c367ad93e2ff8dff02129c33a2e605834c4ff))

#### 1.3.4 (2017-08-21)

##### Continuous Integration

* **npm:** fix auth ([65d10e00](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/65d10e00d216fe2f9009f76466a05cd4f955ca7b))

#### 1.3.3 (2017-08-21)

#### 1.3.2 (2017-08-21)

##### Continuous Integration

* **vulnerabilities:**
  * fix snyk ([45547a85](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/45547a85366d589d17ceaaa1b9289e8886334fa8))
  * fix snyk ([c3567388](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/c356738819196c1a7a2a1b604115ee7bf14752a8))

#### 1.3.1 (2017-08-21)

##### Continuous Integration

* **vulnerabilities:** fix snyk ([c3567388](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/c356738819196c1a7a2a1b604115ee7bf14752a8))

#### 1.3.0 (2017-08-21)

##### Other Changes

* **codestyle:**
  * add eslint config ([82a01468](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/82a01468367c912d07cbd01903e6de28d9951978))
  * add eslint config ([dede252c](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/dede252c7d78c815cdd48bd3c69ac69a007e2a7f))
  * add eslint config ([48caa046](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/48caa046763096bbbc2e7ceeae675478dd92a20f))
* **ignore:** .gitignore update ([66d8b870](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/66d8b870904272b1964c52dbfad48ed9f7563d93))

#### 1.2.0 (2017-08-21)

##### Other Changes

* **codestyle:**
  * add eslint config ([dede252c](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/dede252c7d78c815cdd48bd3c69ac69a007e2a7f))
  * add eslint config ([48caa046](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/48caa046763096bbbc2e7ceeae675478dd92a20f))
* **ignore:** .gitignore update ([66d8b870](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/66d8b870904272b1964c52dbfad48ed9f7563d93))

#### 1.1.1 (2017-08-21)

##### Other Changes

* **packages:** remove package-lock.json ([77c28b61](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/77c28b61e2c5600103b6d5ee0380550a810df49b))

#### 1.1.0 (2017-08-21)

##### Continuous Integration

* **packages:** fix npm installation ([9ad2b046](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/9ad2b0463c43d4687af72bf31a1e520fdc6a2735))

#### 1.0.2 (2017-08-21)

##### Other Changes

* **changelog:** add changelog ([a0c5f117](git+ssh://git@gitlab.com/m03geek/gitlab-workflow-demo.git/commit/a0c5f1177929ebba44989bd8b0666fc580cd256f))

