FROM mhart/alpine-node:latest

RUN apk add --update harfbuzz --repository http://dl-3.alpinelinux.org/alpine/edge/main --allow-untrusted \
 && apk add --update chromium --repository http://dl-3.alpinelinux.org/alpine/edge/community/ --allow-untrusted \
 && apk add udev ttf-freefont

RUN apk del --purge --force curl make gcc g++ python linux-headers binutils-gold gnupg git zlib-dev apk-tools libc-utils \
 && rm -rf /var/lib/apt/lists/* /var/cache/apk/* /usr/share/man

ENV NODE_ENV production
ENV SVC_PATH /opt/service
ENV NODE_INSPECT ""

WORKDIR $SVC_PATH
COPY . $SVC_PATH

EXPOSE 9000
EXPOSE 9229

CMD node --expose-gc $NODE_INSPECT index.js --name=gitlab-workflow-demo
